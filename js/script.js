"use strict"

/* 
Теоретичні питання:



1. 
Опишіть, як можна створити новий HTML тег на сторінці.

До того, як устромляти тег на сторінку, треба якось створити елемент в джаваскрипті.
І вже після цього можна вставити його, при чому позицію буде задано відносно якогось іншого "таргетного" елемента, який вже є в DOM:

targetElement.insertAdjacentHTML(where, htmlAsString);
targetElement.insertAdjacentElement(where, newElement);
targetElement.before(newElement);
targetElement.prepend(newElement);
targetElement.append(newElement);
targetElement.after(newElement);
*/



/*
2. 
Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

Він означє, куди саме відносно "таргетного" елементу DOM буде додано те, що вказано у другому параметрі функції insertAdjacentHTML. На сьогодні JS пропонує чотири варіанти першого параметра — beforebegin, afterbegin, beforeend, afterend. Варіанти beforebegin та afterend не спрацюють, якщо у "таргетного" елемента немає батьківського елемента.
Якщо ми напишемо такий код:   
*/
// const targetElement = document.querySelector("#question2");
// targetElement.insertAdjacentHTML("beforebegin", "<span>---beforebegin!!!</span>");
// targetElement.insertAdjacentHTML("afterbegin", "<span>---afterbegin---</span>");
// targetElement.insertAdjacentHTML("beforeend", "<span>---beforeend---</span>");
// targetElement.insertAdjacentHTML("afterend", "<span>---afterend---</span>");
/* 
то ось куди попаде те, що ми вставляємо, відносно "таргетного" елемента <p id="#question2">:

---beforebegin---
<p id="#question2">
	---afterbegin---
	...контент, що вже був до нас...
	---beforeend---
</p>
---afterend---
*/



/*
3. 
Як можна видалити елемент зі сторінки?

Наступний код видалить тег з класом "existing" зі сторінки:
// document.querySelector(".existing").remove();
*/






// Завдання:



// задаємо необовʼязковий обʼєкт, щоб потім додати список не до body, а до нього:
const targetEl = document.querySelector(".insert-here");

const timer = document.querySelector(".timer");

const myList = ["Харків", "Київ", ["Бориспіль", "Ірпінь",["Ірпінь-1", "Ірпінь-2", "Ірпінь-3"]], "Одесса", "Львів", "Дніпро"];

function arrayToLi(array, targetElement = document.body) {
	// якщо цій функції передадуть не масив, доцільно видати помилку:
	if (!Array.isArray(array)) {
		throw new Error("Argument 1 is not an array.");
	};

	// якщо функція отримала масив, тоді можна починати:
	const ul = document.createElement("ul");

	array.forEach(el => {
		// якщо елемент масива — масив, запустимо рекурсію:
		if (Array.isArray(el)) {
			arrayToLi(el,ul);
		} else {
			ul.insertAdjacentHTML("beforeend", `<li>${el}</li>`);
		}
	});

	targetElement.append(ul);
	// треба повернути створений елемент, щоби потім можна було видалити саме його:
	return ul;
};

// Врешті, додаємо створений список на сторінку. Другий аргумент функції arrayToLi не є обовʼязковим. 
// Повертаємо обʼєкт "ul" у змінну, щоб потім не довелося видаляти батьківський targetElement, який може бути document.body при виклику arrayToLi без другого аргументу:
let whatToRemove = arrayToLi(myList, targetEl);

// додаємо відлік, після якого видалимо все, що створили, але не більше:
let sec = 3;
let ticker = setInterval(removeAfter,1000);

// замість повного очищення сторінки методом "document.body.innerHTML='';" я краще видалю лише ті обʼєкти, які створив мій код (а саме список та сам покажчик відліку):
function removeAfter() {
	sec--; 
	timer.innerHTML = sec;
	if(sec <= 0){
		// видаляємо елемент, створений функцією arrayToLi:
		whatToRemove.remove();
		// видаляємо покажчик відліку
		timer.remove();

		// зупиняємо вже непотрібний setInterval:
		clearInterval(ticker);
	};
};